package com.learning.kubernetes.service;

import com.learning.kubernetes.dto.DiceRollOutput;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@Service
public class DiceService {

    public List<DiceRollOutput> rollDice(Integer noOfDice) {
        Random random = new Random();
        return IntStream.range(1, noOfDice + 1)
                .mapToObj(i -> new DiceRollOutput(i, random.nextInt(1, 6))).toList();
    }
}
