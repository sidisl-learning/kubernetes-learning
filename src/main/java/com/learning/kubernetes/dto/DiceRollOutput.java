package com.learning.kubernetes.dto;

public record DiceRollOutput(Integer diceNo, Integer value) {
}