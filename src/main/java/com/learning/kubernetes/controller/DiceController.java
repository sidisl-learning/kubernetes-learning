package com.learning.kubernetes.controller;

import com.learning.kubernetes.dto.DiceRollOutput;
import com.learning.kubernetes.service.DiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dice")
public class DiceController {

    @Autowired
    private DiceService diceService;

    @GetMapping("/{noOfDice}/roll")
    public List<DiceRollOutput> rollDice(@PathVariable(name = "noOfDice") Integer noOfDice) {
        return diceService.rollDice(noOfDice);
    }
}
